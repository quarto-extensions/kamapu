# Beamer for kamapu.net

A template for Beamer documents using Quarto.

## Installieren

Die Installation in ein bestehendes Projekt erfolgt über folgende Kommandozeile im Terminal:

```bash
quarto add https://gitlab.com/quarto-extensions/kamapu/-/archive/main/shorty-main.zip
```

Um eine Template-Installation durchzuführen, die auch Templates enthält, verwende folgende Befehlszeile.
Dieser Schritt erstellt einen neuen Ordner, der die Vorlagen und die Erweiterung enthält.

```bash
quarto use template https://gitlab.com/quarto-extensions/kamapu/-/archive/main/shorty-main.zip
```

## Usage

For **beamer**:

```yaml
format: kamapu-beamer
```

## Examples

Here there is an example distributed with the source code:

- **beamer** ([Source code](example-beamer.qmd), [Preview](example-beamer.pdf))
